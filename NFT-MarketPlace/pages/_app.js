/* pages/_app.js */
import '../styles/globals.css'
import Link from 'next/link'
import { ethers } from 'ethers'
import { useEffect, useState } from 'react'
import axios from 'axios'
import Web3Modal from 'web3modal'
import { useRouter } from 'next/router'

import {
  marketplaceAddress
} from '../config'

import NFTMarketplace from '../artifacts/contracts/NFTMarketplace.sol/NFTMarketplace.json'






function MyApp({ Component, pageProps }) {
  

  const AddressNav =  () => {

  
    let [account, setAccount] = useState(null);
      
      useEffect(() => {
        loadNavBar();
      }, [account]);
      
      let loadNavBar = async () => {
        const accounts = await window.ethereum.request({
          method: "eth_requestAccounts",
        });
        setAccount(accounts[0]);
      };
      return account;
}

  return (
    <div>

      <nav className="border-b p-6 text-center">
        <p className="text-4xl  font-bold">NFT MarketPlace</p> <span className='text-red-600'> {AddressNav()}</span>


        <div className="mt-10 text-center">
          <Link href="/">
            <a className="mr-4 text-black-500">
              Home
            </a>
          </Link>
          <Link href="/create">
            <a className="mr-6 text-black-500">
              Sell NFT
            </a>
          </Link>
          <Link href="/portfolio">
            <a className="mr-6 text-black-500">
              My Portfolio
            </a>
          </Link>
          <Link href="/dashboard">
            <a className="mr-6 text-black-500">
              Dashboard
            </a>
          </Link>
          <a className="mr-4 text-black-500">
             
            </a>
        </div>
      </nav>
      <Component {...pageProps} />
    </div>
  )
}

export default MyApp